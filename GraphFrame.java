import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

@SuppressWarnings("serial")
public class GraphFrame extends JFrame implements Observer, KeyListener {
	
	private GraphPanel graphPanel;
	private String filePath;
	private JLabel textLabel;
	private JMenuItem removeVertexItem;
	private JMenuItem addEdgeItem;
	private JMenuItem removeEdgeItem;
	private JMenuItem renameItem;
	private JMenuItem resizeItem;
	private GraphVertex selectedVertex;
	
	public GraphFrame() {
		// Creating the frame
		super("Graph Editor");
		createMenuBar();
		createStatusBar();
		graphPanel = new GraphPanel();
		add(graphPanel);
		graphPanel.getSelection().addObserver(this);
		addKeyListener(this);
		selectedVertex = null;
		addEdgeItem.setEnabled(false);
		removeVertexItem.setEnabled(false);
		removeEdgeItem.setEnabled(false);
		renameItem.setEnabled(false);
		resizeItem.setEnabled(false);
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
	}
	
	public void duplicate(){
		GraphFrame frame = new GraphFrame();
		GraphPanel panel = new GraphPanel();
		graphPanel.addPanelListener(panel);
		panel.addPanelListener(graphPanel);
		panel.setModel(graphPanel.getModel());
		frame.add(panel);
		frame.setVisible(true);
		panel.revalidate();
		panel.repaint();
	}
	
	/**
	 * Creating a menu bar with the tabs "file", "edit", "view" and "help"
	 * each with graph options belonging to the menu, including fancy separators.
	 */
	private void createMenuBar(){
		// Initialising the menubar
		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		JMenu view = new JMenu("View");
		JMenu help = new JMenu("Help");
		JMenuItem newItem = new JMenuItem("New");
		JMenuItem openItem = new JMenuItem("Open...");
		JMenuItem saveItem = new JMenuItem("Save");
		JMenuItem saveAsItem = new JMenuItem("Save As...");
		JMenuItem addVertexItem = new JMenuItem("Add Vertex");
		JMenuItem refreshItem = new JMenuItem("Refresh");
		JMenuItem searchItem = new JMenuItem("Search Vertex...");
		JMenuItem duplicateItem = new JMenuItem("Duplicate Frame");
		JMenuItem shortcutsItem = new JMenuItem("Shortcuts");
		JMenuItem aboutItem = new JMenuItem("About");

		this.addEdgeItem = new JMenuItem("Add Edge");
		this.removeVertexItem = new JMenuItem("Delete Vertex");
		this.removeEdgeItem = new JMenuItem("Delete Edge");
		this.renameItem = new JMenuItem("Rename...");
		this.resizeItem = new JMenuItem("Resize...");
		
		/**
		 * Actions for items under "file" in the menu bar.
		 */
		newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuNew();
            }
        });
		
		openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuOpen();
            }
        });
		
		saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuSave();
            }
        });
		
		saveAsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuSaveAs();
            }
        });
		
		/**
		 * Actions for items under "edit" in the menu bar.
		 */
		
		addVertexItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAddVertex();
            }
        });
		
		addEdgeItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAddEdge();
            }
        });
		
		removeVertexItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuRemoveVertex();
            }
        });
		
		removeEdgeItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuRemoveEdge();
            }
        });
		
		renameItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuRename();
            }
        });
		
		resizeItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuResize();
            }
        });
		
		/**
		 * Actions for items under "view" in the menu bar.
		 */
		
		searchItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuSearch();
            }
        });
		
		refreshItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuRefresh();
            }
        });
		
		duplicateItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuDuplicate();
            }
        });
		
		/**
		 * Actions for items under "help" in the menu bar.
		 */
		
		shortcutsItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuShortcuts();
            }
        });
		
		aboutItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                menuAbout();
            }
        });
		
		// Finalising the menubar
		file.add(newItem);
		file.add(openItem);
		file.addSeparator();
		file.add(saveItem);
		file.add(saveAsItem);
		menuBar.add(file);
		edit.add(addVertexItem);
		edit.add(addEdgeItem);
		edit.add(removeVertexItem);
		edit.add(removeEdgeItem);
		edit.addSeparator();
		edit.add(resizeItem);
		edit.add(renameItem);
		menuBar.add(edit);
		view.add(searchItem);
		view.add(refreshItem);
		view.add(duplicateItem);
		menuBar.add(view);
		help.add(shortcutsItem);
		help.add(aboutItem);
		menuBar.add(help);
		this.setJMenuBar(menuBar);
	}
	
	/**
	 * Creating a status bar at the bottom which shows text to inform
	 * the user about actions that are or should be performed
	 */
	private void createStatusBar(){
		JPanel textPanel = new JPanel();
		textPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		add(textPanel, BorderLayout.SOUTH);
		textPanel.setPreferredSize(new Dimension(getWidth(), 20));
		textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.X_AXIS));
		textLabel = new JLabel("");
		textLabel.setHorizontalAlignment(SwingConstants.LEFT);
		textPanel.add(textLabel);
	}
	
	/**
	 * Methods for actions under "file" in the menu bar.
	 */
	
	private void menuNew(){
		graphPanel.setModel(new GraphModel());
		setText("New graph created!");
		selectedVertex = null;
		graphPanel.setSelectedVertex(null);
		repaint();
	}
	
	private void menuOpen(){
		// Use the JFileChooser to find the path to a user defined file to load
		setText("Opening file...");
		JFileChooser chooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(
	        ".txt files", "txt");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showOpenDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	// Clear current model, only then load new model
	    	menuNew();
	    	filePath = chooser.getSelectedFile().getAbsolutePath();
	    	graphPanel.getModel().load(filePath);
	    	setText("Graph loaded!");
	    } else {
	    	setText("Opening cancelled!");
	    }
	    revalidate();
	    repaint();
	}
	
	private void menuSave(){
		if(filePath==null){
			// If no file has been opened yet, "save" button acts as "save as" button
			menuSaveAs();
		} else {
			graphPanel.getModel().save(filePath);
			setText("File successfully saved to " + filePath);
		}
	}
	
	private void menuSaveAs(){
		// Use the JFileChooser to find the path to a user defined file to load
		setText("Saving file...");
		JFileChooser chooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(
	        ".txt files", "txt");
	    chooser.setFileFilter(filter);
	    int returnVal = chooser.showSaveDialog(null);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	// Make sure the file will be properly saved with extension
	    	String path = chooser.getSelectedFile().getAbsolutePath();
	    	if(!path.endsWith(".txt")){
	    		path += ".txt";
	    	}
	    	filePath = path;
	    	graphPanel.getModel().save(filePath);
	    	setText("File successfully saved to " + filePath);
	    } else {
	    	setText("Saving cancelled!");
	    }
	    revalidate();
	    repaint();
	}
	
	/**
	 * Methods for actions under "edit" in the menu bar.
	 */
	
	private void menuAddVertex(){
		graphPanel.getModel().addGraphVertex();
		graphPanel.getSelection().setSelectedVertex(null);
		setText("Vertex added!");
	}
	
	private void menuAddEdge(){
		setText("Please select a second vertex to connect to...");
		graphPanel.getSelection().setStatus(1);
	}
	
	public void menuRemoveVertex(){
		graphPanel.getModel().removeGraphVertex(selectedVertex);
		graphPanel.getSelection().setSelectedVertex(null);
		setText("Vertex removed!");
	}
	
	public void menuRemoveEdge(){
		setText("Please select a second vertex to disconnect from...");
		graphPanel.getSelection().setStatus(2);
	}
	
	public void menuRename(){
		String name = JOptionPane.showInputDialog("Rename \"" + selectedVertex.getName() + "\" to...", "New vertex");
		if(name != null){
			selectedVertex.setName(name);
			List<GraphVertex> graphVertex = graphPanel.getModel().getListGraphVertex();
			graphVertex.set(graphPanel.getSelection().getSelectedVertexIndex(), selectedVertex);
	        graphPanel.getModel().setListGraphVertex(graphVertex);
	        graphPanel.getSelection().setSelectedVertex(null);
		}
        revalidate();
        repaint();
	}
	
	public void menuResize(){
		JTextField width = new JTextField();
		JTextField height = new JTextField();
		Object[] options = {
		    "New width (currently " + (int)selectedVertex.getRectangle().getWidth() + "):", width,
		    "New height (currently " + (int)selectedVertex.getRectangle().getHeight() + "):", height,
		};
		int option = JOptionPane.showConfirmDialog(null, options, "Resize vertex", JOptionPane.OK_CANCEL_OPTION);
		if (option == JOptionPane.OK_OPTION) {
		    String w = width.getText();
		    String h = height.getText();
		    // Initializing rectangle size variables: just in case, not initializing at size 0
		    int newWidth = 50, newHeight = 50;
		    try { 
		        newWidth = Integer.parseInt(w); 
		        newHeight = Integer.parseInt(h);
		    } catch(NumberFormatException e) { 
		        System.err.println("Invalid rectangle size.");
		        return;
		    } catch(NullPointerException e) {
		        System.err.println("Invalid rectangle size.");
		        return;
		    }
		    selectedVertex.setRectangle((int)selectedVertex.getRectangle().getX(), (int)selectedVertex.getRectangle().getY(), newWidth, newHeight);
		    List<GraphVertex> graphVertex = graphPanel.getModel().getListGraphVertex();
			graphVertex.set(graphPanel.getSelection().getSelectedVertexIndex(), selectedVertex);
	        graphPanel.getModel().setListGraphVertex(graphVertex);
	        graphPanel.getSelection().setSelectedVertex(null);
		}
        revalidate();
        repaint();
	}
	
	/**
	 * Methods for actions under "view" in the menu bar.
	 */
	
	private void menuSearch(){
		GraphVertex partialMatch = null;
		GraphVertex perfectMatch = null;
		graphPanel.getSelection().setSelectedVertex(null);
		String name = JOptionPane.showInputDialog("Search for vertex...", "Vertex name");
		if(name != null){
			Iterator<GraphVertex> vertices = graphPanel.getModel().getListGraphVertex().iterator();
			while(vertices.hasNext()){
				GraphVertex vertex = vertices.next();
				if(vertex.getName().equalsIgnoreCase(name)){
					perfectMatch = vertex;
					break;
				} else if(vertex.getName().toLowerCase().startsWith(name.toLowerCase())){
					partialMatch = vertex;
				}
			}
			if(perfectMatch != null){
				graphPanel.getSelection().setSelectedVertex(perfectMatch);
			} else if(partialMatch != null){
				graphPanel.getSelection().setSelectedVertex(partialMatch);
			} else {
				setText("Could not find vertex \"" + name + "\"");
			}
		}
	}
	
	private void menuRefresh(){
		// Simple tool to make sure everything that is drawn is refreshed
		revalidate();
		repaint();
		graphPanel.revalidate();
		graphPanel.repaint();
		setText("That was refreshing!");
	}
	
	private void menuDuplicate(){
		duplicate();
		graphPanel.notifyListeners();
		setText("Frame duplicated");
	}
	
	/**
	 * Methods for actions under "help" in the menu bar.
	 */
	
	private void menuShortcuts(){
		JOptionPane.showMessageDialog(null, "CTRL + N:                   Create new graph\nCTRL + O:                   Open graph from file\nCTRL + S:                   Save graph\nCTRL + SHIFT + S:    Save graph as...\n\nCTRL + V:                   Add vertex\nCTRL + SHIFT + V:    Remove vertex\nDELETE:                      Remove vertex\nCTRL + E:                   Add edge\nCTRL + SHIFT + E:    Remove edge\n\nCTRL + R:                   Rename vertex\nCTRL + Z:                   Resize vertex\nCTRL + F:                   Search vertex\nF5:                               Refresh", "Shortcuts", JOptionPane.PLAIN_MESSAGE);
	}
	
	private void menuAbout(){
		JOptionPane.showMessageDialog(null, "This program was created by Kevin Nauta & Herman Groenbroek.\n" + "This is our solution to an assignment for Object Oriented Programming.\n" + "� 2015", "Graph Editor v1.0", JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Getters and setters for manipulating the file path.
	 * @param path is the path to the file that was last opened; null if no file has been opened
	 */
	
	public void setFilePath(String path){
		filePath = path;
	}
	
	public String getFilePath(){
		return filePath;
	}
	
	public void setText(String text){
		textLabel.setText(text);
	}
	
	private void ChangeEditingMenuItems(GraphVertex selectedVertex){
		if(selectedVertex != null){
			// Vertex selected; show following editing items
			addEdgeItem.setEnabled(true);
			removeVertexItem.setEnabled(true);
			removeEdgeItem.setEnabled(true);
			renameItem.setEnabled(true);
			resizeItem.setEnabled(true);
		} else {
			// No vertex selected; following editing items are not relevant
			addEdgeItem.setEnabled(false);
			removeVertexItem.setEnabled(false);
			removeEdgeItem.setEnabled(false);
			renameItem.setEnabled(false);
			resizeItem.setEnabled(false);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// The selected vertex, Object arg, is updated (it's the only object ever given through the notifier, which is why casting is safe)
		selectedVertex = (GraphVertex) arg;
		if(selectedVertex != null){
			setText("\"" + selectedVertex.getName() + "\" selected!");
		} else {
			setText("");
		}
		ChangeEditingMenuItems(selectedVertex);
		revalidate();
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// All keyboard shortcuts are defined here
		control:
		if(e.isControlDown()){
			if(e.isShiftDown()){
				// CTRL + SHIFT + <KEY>
				if(e.getKeyCode() == KeyEvent.VK_S){
					menuSaveAs();
					break control;
				} else if(e.getKeyCode() == KeyEvent.VK_E && selectedVertex != null){
					menuRemoveEdge();
					break control;
				} else if(e.getKeyCode() == KeyEvent.VK_V && selectedVertex != null){
					menuRemoveVertex();
					break control;
				}
				// CTRL + <KEY>
			} else if(e.getKeyCode() == KeyEvent.VK_N){
				menuNew();
			} else if(e.getKeyCode() == KeyEvent.VK_S){
				menuSave();
			} else if(e.getKeyCode() == KeyEvent.VK_O){
				menuOpen();
			} else if(e.getKeyCode() == KeyEvent.VK_V){
				menuAddVertex();
			} else if(e.getKeyCode() == KeyEvent.VK_F){
				menuSearch();
			} else if(e.getKeyCode() == KeyEvent.VK_E && selectedVertex != null){
				menuAddEdge();
			} else if(e.getKeyCode() == KeyEvent.VK_R && selectedVertex != null){
				menuRename();
			} else if(e.getKeyCode() == KeyEvent.VK_Z && selectedVertex != null){
				menuResize();
			}
			// <KEY>
		} else if(e.getKeyCode() == KeyEvent.VK_DELETE){
			menuRemoveVertex();
		} else if(e.getKeyCode() == KeyEvent.VK_F5){
			menuRefresh();
		} else {
			setText("");
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// Empty: do nothing
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// Empty: do nothing
	}

}
