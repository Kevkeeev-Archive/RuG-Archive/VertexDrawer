import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import java.util.Observable;



public class SelectionController extends Observable implements MouseListener, MouseMotionListener {
	
	private GraphModel model;
	private GraphPanel graphPanel;
	private GraphVertex selectedVertex; // Selected vertex not in the model to keep assignment part 1 and 2 more separated
	private int selectedVertexIndex;
	private int xPressed;
	private int yPressed;
	private int status; // 0: default; 1: add Edge; 2: delete Edge;
	private boolean dragging;
	
	public SelectionController(){
		selectedVertex = null;
		selectedVertexIndex = -1;
		status = 0;
		dragging = false;
	}
	
	public void setModel(GraphModel model){
		this.model = model;
	}
	
	public GraphModel getModel(){
		return model;
	}
	
	public void setSelectedVertex(GraphVertex vertex){
		this.selectedVertex = vertex;
		if(vertex == null){
			this.selectedVertexIndex = -1;
		}
		setChanged();
		notifyObservers(selectedVertex);
	}
	
	public int getSelectedVertexIndex(){
		return selectedVertexIndex;
	}
	
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public void setGraphPanel(GraphPanel panel){
		graphPanel = panel;
	}
	
	
	/**
	 * Selects the first vertex in the list at the location of the click event
	 * (Edges need not be selectable, as they should not be dragged and can be removed in the "edit" menu)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Empty: do nothing
	}

	@Override
	public void mousePressed(MouseEvent e) {
		graphPanel.setNewEdge(false);
		xPressed = e.getX();
		yPressed = e.getY();
		int i = 0;
		Iterator<GraphVertex> vertices = model.getListGraphVertex().iterator();
		boolean vertexSelected = false;
		whileloop: // Naming the while loop for breaking: no issues when clicking intersecting vertices now
		while(vertices.hasNext()){
			// i is the index number of the current vertex
			GraphVertex vertex = vertices.next();
			if((yPressed > vertex.getRectangle().getY() && yPressed < (vertex.getRectangle().getY() + vertex.getRectangle().getHeight())) && (xPressed > vertex.getRectangle().getX() && xPressed < (vertex.getRectangle().getX() + vertex.getRectangle().getWidth()))){
				// Current vertex is the first vertex in the list that is at mouse pointer location
				if(status == 1 && i != selectedVertexIndex){
					// Add an edge from selected vertex to this newly selected vertex
					// Note that we do allow multiple edges between the same vertices, in case we were to allow directed graphs
					model.addGraphEdge(new GraphEdge(i, selectedVertexIndex));
					status = 0;
				} else if(status == 2 && i != selectedVertexIndex){
					// Delete an edge
					Iterator<GraphEdge> edges = model.getListGraphEdge().iterator();
					while(edges.hasNext()){
						GraphEdge edge = edges.next();
						if((edge.getSideOne() == i && edge.getSideTwo() == selectedVertexIndex) || (edge.getSideTwo() == i && edge.getSideOne() == selectedVertexIndex)){
							model.removeGraphEdge(edge);
							status = 0;
							break whileloop;
						}
					}
				}
				vertexSelected = true;
				selectedVertex = vertex;
				selectedVertexIndex = i;
				status = 0;
				break whileloop;
			}
			if(!vertexSelected){
				selectedVertex = null;
			}
			i++;
		}
		setChanged();
		notifyObservers(selectedVertex);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		dragging = false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(selectedVertex != null){
			// Adjust for mouse pointer location in the rectangle
			if(!dragging){
				xPressed -= selectedVertex.getRectangle().getX();
				yPressed -= selectedVertex.getRectangle().getY();
				
			}
			dragging = true;
			// Get the actual vertex from the model to alter location
			GraphVertex vertex = model.getListGraphVertex().get(selectedVertexIndex);
			vertex.setRectangle((int)(e.getX() - xPressed), (int)(e.getY() - yPressed), (int)vertex.getRectangle().getWidth(), (int)vertex.getRectangle().getHeight());
			selectedVertex = vertex;
			setChanged();
			notifyObservers(selectedVertex);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		if(status == 1){
			graphPanel.setNewEdge(true);
			graphPanel.setCoordinates(e.getX(), e.getY());
			setChanged();
			notifyObservers(selectedVertex);
		}
	}
	
}
