import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GraphPanel extends JPanel implements Observer {

	private GraphModel model;
	private SelectionController selection;
	private GraphVertex selectedVertex;
	private boolean newEdge;
	private int x, y;
	private ArrayList<GraphPanel> panelListeners;
	
	public GraphPanel() {
		this.setModel(new GraphModel());
		model.addObserver(this);
		panelListeners = new ArrayList<>();
		
		// Enabling the selection controller
		if(panelListeners.isEmpty()){
			selection = new SelectionController();
			selection.setModel(getModel());
		} else {
			selection = panelListeners.get(0).getSelection();
		}
		addMouseListener(selection);
		addMouseMotionListener(selection);
		selection.addObserver(this);
		selection.setGraphPanel(this);
		newEdge = false;
	}
	
	public void addPanelListener(GraphPanel listener){
		panelListeners.add(listener);
	}
	
	public ArrayList<GraphPanel> getPanelListeners(){
		return panelListeners;
	}
	
	public void notifyListeners(){
		// Notify listener panels by giving them this updated panel
		for(GraphPanel listener : panelListeners){
			listener.onNotify(this);
		}
	}
	
	public void onNotify(GraphPanel panel){
		System.out.println("Panel " + this.hashCode() + " was notified!");
		setModel(panel.getModel());
		revalidate();
		repaint();
	}
	
	public void setModel(GraphModel model){
		model.deleteObserver(this);
		this.model = model;
		model.addObserver(this);
		repaint();
	}
	
	public GraphModel getModel(){
		return model;
	}
	
	public SelectionController getSelection(){
		return selection;
	}

	public void setSelectedVertex(GraphVertex vertex) {
		this.selectedVertex = vertex;
	}
	
	public void setCoordinates(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public void setNewEdge(boolean value){
		newEdge = value;
	}
	
	public void paintComponent(Graphics g){
		if(newEdge){
			paintAddingEdge(g);
		}
		paintEdges(g);
		paintVertices(g);
	}
	
	private void paintVertices(Graphics g){
		// Paint the graph vertices in reverse order, so the apparent top vertex is draggable in the program
		ListIterator<GraphVertex> vertices = model.getListGraphVertex().listIterator(model.getListGraphVertex().size());
		int x, y;
		while(vertices.hasPrevious()){
			GraphVertex vertex = vertices.previous();
			// Fill vertex rectangle with a colour first
			if(vertex == selectedVertex){
				g.setColor(new Color(25, 255, 70)); // That's green
			} else {
				g.setColor(Color.white);
			}
			g.fillRect((int)vertex.getRectangle().getX(), (int)vertex.getRectangle().getY(), (int)vertex.getRectangle().getWidth(), (int)vertex.getRectangle().getHeight());
			// Draw the vertex rectangle
			g.setColor(Color.black);
			g.drawRect((int)vertex.getRectangle().getX(), (int)vertex.getRectangle().getY(), (int)vertex.getRectangle().getWidth(), (int)vertex.getRectangle().getHeight());
			// Store the width of the vertex name
			int stringWidth = g.getFontMetrics().stringWidth(vertex.getName());
			// Set the x and y locations such that the string prints perfectly centered
			x = ((int)vertex.getRectangle().getWidth() - stringWidth) / 2 + (int)vertex.getRectangle().getX();
			y = (int)vertex.getRectangle().getY() + (int)vertex.getRectangle().getHeight()/2 + g.getFontMetrics().getAscent()/2;
			
			g.drawString(vertex.getName(), x, y);
		}
	}
	
	private void paintEdges(Graphics g){
		Iterator<GraphEdge> edges = model.getListGraphEdge().iterator();
		int xOne, yOne, xTwo, yTwo;
		while(edges.hasNext()){
			GraphEdge edge = edges.next();
			GraphVertex vertexOne = model.getListGraphVertex().get(edge.getSideOne());
			GraphVertex vertexTwo = model.getListGraphVertex().get(edge.getSideTwo());
			xOne = (int)(vertexOne.getRectangle().getX() + vertexOne.getRectangle().getWidth()/2);
			yOne = (int)(vertexOne.getRectangle().getY() + vertexOne.getRectangle().getHeight()/2);
			xTwo = (int)(vertexTwo.getRectangle().getX() + vertexTwo.getRectangle().getWidth()/2);
			yTwo = (int)(vertexTwo.getRectangle().getY() + vertexTwo.getRectangle().getHeight()/2);
			g.drawLine(xOne, yOne, xTwo, yTwo);
		}
	}
	
	public void paintAddingEdge(Graphics g) {
		if(selectedVertex != null){
			int xOne = (int) (selectedVertex.getRectangle().getX() + 0.5 * selectedVertex.getRectangle().getWidth());
			int yOne = (int) (selectedVertex.getRectangle().getY() + 0.5 * selectedVertex.getRectangle().getHeight());
			g.drawLine(xOne, yOne, x, y);
		}
	}


	@Override
	public void update(Observable o, Object arg) {
		// When something in the model changes, change the model field of the selectioncontroller as well.
		selection.setModel(getModel());
		// Argument can only be a GraphVertex in the program.
		selectedVertex = (GraphVertex)arg;
		notifyListeners();
		revalidate();
		repaint();
	}
	
}
