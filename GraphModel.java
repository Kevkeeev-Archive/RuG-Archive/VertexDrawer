
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class GraphModel extends Observable {
    private List<GraphVertex> listGraphVertex;
    private List<GraphEdge> listGraphEdge;
    
    public GraphModel() {
    	listGraphVertex = new ArrayList<GraphVertex>();
    	listGraphEdge = new ArrayList<GraphEdge>();
    }
    
    public GraphModel(String filename) {
    	listGraphVertex = new ArrayList<GraphVertex>();
    	listGraphEdge = new ArrayList<GraphEdge>();
    	this.load(filename);
    }

	public List<GraphVertex> getListGraphVertex() {
		return listGraphVertex;
	}

	public void setListGraphVertex(List<GraphVertex> listGraphVertex) {
		this.listGraphVertex = listGraphVertex;
		setChanged();
		notifyObservers();
	}

	public List<GraphEdge> getListGraphEdge() {
		return listGraphEdge;
	}

	public void setListGraphEdge(List<GraphEdge> listGraphEdge) {
		this.listGraphEdge = listGraphEdge;
		setChanged();
		notifyObservers();
	}
	
	// Overloading [1/2]
	public void addGraphVertex() {
		this.listGraphVertex.add(new GraphVertex());
		setChanged();
		notifyObservers();
	}
	
	// Overloading [2/2]
	public void addGraphVertex(GraphVertex vertex) {
		this.listGraphVertex.add(vertex);
		setChanged();
		notifyObservers();
	}
	
	public void addGraphEdge(GraphEdge edge) {
		this.listGraphEdge.add(edge);
		setChanged();
		notifyObservers(null);
	}
	
	// Removing a vertex means that we have to remove all edges belonging to this vertex as well.
	public void removeGraphVertex(GraphVertex deleteVertex){
		// Get the list number of the vertex to be removed
		Iterator<GraphVertex> vertexIterator = listGraphVertex.iterator();
		for(int i=0; i<listGraphVertex.size(); i++){
			GraphVertex vertex = vertexIterator.next();
			if(vertex == deleteVertex){
				// Do something with this vertex to be deleted
				// i equals the vertex index to be removed
				Iterator<GraphEdge> edgeIterator = listGraphEdge.iterator();
				List<GraphEdge> newListEdges = new ArrayList<>();
				while (edgeIterator.hasNext()) {
					GraphEdge edge = edgeIterator.next();
					// Remove edge if it connects to the vertex to be deleted
					if(edge.getSideOne() == i || edge.getSideTwo() == i) {
						edgeIterator.remove();
					} else {
						// Decrement edge references to vertices with an index that is now one less
						if(edge.getSideOne() > i){
							edge.setSideOne(edge.getSideOne() - 1);
						}
						if(edge.getSideTwo() > i){
							edge.setSideTwo(edge.getSideTwo() - 1);
						}
						newListEdges.add(edge);
					}
				}
				// Finally, replace the list of edges and remove the vertex itself
				setListGraphEdge(newListEdges);
				listGraphVertex.remove(i);
				setChanged();
				// Selected vertex does not exist anymore: will point to null
				notifyObservers(null);
				break;
			}
		}
	}
	
	public void removeGraphEdge(GraphEdge edge){
		listGraphEdge.remove(edge);
		setChanged();
		notifyObservers(null);
	}
	
	/**
	 * Loads the graph model from a file
	 * @param filename is the filename and extension to save the file in
	 */
	public void load(String filename){
		
		StringTokenizer token = null;
		BufferedReader input = null;
		
		try {
			input = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			System.err.println("FILE NOT FOUND! Please check if you supplied the right file!");
			e.printStackTrace();
			System.exit(-1);
		}

		int i, numVertices=0, numEdges=0;
		
		try {
			token = new StringTokenizer (input.readLine());
			numVertices = Integer.parseInt(token.nextToken());
			numEdges = Integer.parseInt(token.nextToken());
		} catch (NoSuchElementException e) {
			System.err.println("Could not determine list lengths. Input file possibly corrupted. Please check.");
			e.printStackTrace();
			System.exit(-2);
		} catch (IOException e) {
			System.err.println("Something went wrong probably with \"input.readLine()\". See stacktrace for more details:");
			e.printStackTrace();
			System.exit(-2);
		}
		
		int x, y, width, height;
		String vertexName;
		
		try {
			for(i=0; i<numVertices; i++){
				token = new StringTokenizer(input.readLine());
				x = Integer.parseInt(token.nextToken());
				y = Integer.parseInt(token.nextToken());
				width = Integer.parseInt(token.nextToken());
				height = Integer.parseInt(token.nextToken());
				vertexName = token.nextToken();
				while(token.hasMoreTokens()) {
					// Get full name which may consist of multiple strings
					vertexName += " " + token.nextToken();
				}
				GraphVertex vertex = new GraphVertex(x, y, width, height, vertexName);
				this.addGraphVertex(vertex);
			}
		} catch (NoSuchElementException e) {
			System.err.println("NO MORE TOKENS! Tried to add nonexistent tokens to list. Please check input file.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Something went wrong probably with \"input.readLine()\". See stacktrace for more details:");
			e.printStackTrace();
			System.exit(-3);
		}

		int sideOne, sideTwo;
		
		try {
			for(i=0; i<numEdges; i++){
				token = new StringTokenizer(input.readLine());
				sideOne = Integer.parseInt(token.nextToken());
				sideTwo = Integer.parseInt(token.nextToken());
				this.addGraphEdge(new GraphEdge(sideOne, sideTwo));
			}
		} catch (NoSuchElementException e) {
			System.err.println("NO MORE TOKENS! Tried to add nonexistent tokens to list. Please check the input file.");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Something went wrong probably with \"input.readLine()\". See stacktrace for more details:");
			e.printStackTrace();
			System.exit(-4);
		}
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Saves the graph model into a file
	 * @param filename is the filename and extension to save the file in
	 */
	public void save(String filename){
		BufferedWriter output = null;
		
		try {
			output = new BufferedWriter(new FileWriter(filename));
		} catch (IOException e) {
			System.err.println("Something went wrong. Perhaps there already is a file named '" + filename + "' ? See stacktrace for more details:");
			e.printStackTrace();
		}
		
		int listGraphVertexLength = listGraphVertex.size();
		int listGraphEdgeLength = listGraphEdge.size();
		
		try {
			
			output.write(listGraphVertexLength + " " + listGraphEdgeLength);
			output.newLine();
			
			for(GraphVertex vertex : listGraphVertex) {
				output.write(vertex.toString());
				output.newLine();
			}
			
			for(GraphEdge edge : listGraphEdge) {
				output.write(edge.toString());
				output.newLine();
			}
			
			output.close();
			
		} catch(IOException e){
			System.err.println("Something went wrong with the output file. See stacktrace for more details:"); 
			e.printStackTrace();
		}
	}
  
  public String toString(){
		String str = "Model: " + this.hashCode() + '\n';
		
		str += listGraphVertex.size() + " Vertices:" + '\n';
		for(GraphVertex vertex : listGraphVertex){
			str += vertex.toString() + "\n";
		}
		str += listGraphEdge.size() + " Edges:" + '\n';
		for(GraphEdge edge : listGraphEdge){
			str += edge.toString() + "\n";
		}
		
		return str;
	}
	
}