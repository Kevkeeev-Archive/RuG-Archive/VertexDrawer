
public class GraphEdge {
	/* A GraphEdge connects two, no more and no less, vertices.
	 * They are indicated with an integer: the n'th element of the listGraphVertex in GraphModel.java
	 */
    private int sideOne, sideTwo;
    
    public GraphEdge(int one, int two){
        this.sideOne = one;
        this.sideTwo = two;
    
    }
    
    public int getSideOne(){
        return sideOne;
    }
    
    public int getSideTwo(){
        return sideTwo;
    }
    
    public void setSideOne(int sideOne){
        this.sideOne = sideOne;
    }
    
    public void setSideTwo(int sideTwo){
        this.sideTwo = sideTwo;
    }
    
    public String toString(){
    	return (this.getSideOne() + " " + this.getSideTwo());
    }
    
}